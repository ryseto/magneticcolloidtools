//
//  PairCorrelationFunction.h
//  MagneticColloidTools
//
//  Created by Ryohei Seto on 11/15/15.
//  Copyright © 2015 Ryohei Seto. All rights reserved.
//

#ifndef PairCorrelationFunction_h
#define PairCorrelationFunction_h
#include <vector>
#include <complex>
#include "System.hpp"
#include "vec3d.h"
#include "BoxSet.hpp"
#include "Box.hpp"
#include "Particle.hpp"
#include "Yaplot.h"
using namespace std;



void PairCorrelationFunction(string file_name)
{
	System sys;
//	sys.importData(file_name);
//	sys.importData_exdata(<#ifstream &input_data#>)(file_name);
	string order_parameter_output = "GofR_"+sys.data_name+".dat";
	sys.fout.open(order_parameter_output);
	//estimateSystemSize(sys);
	//outputYapColorRainbow_cout();
	//sys.fout_OPconfig << sys.lnddkwex << ' ' << sys.lz << endl;
	//sys.i_time_max = (int)sys.position.size();
	
	double dr = 0.001;
	double r_min = 1.9;
	double r_max = 4;
	int r_num = (r_max-r_min)/dr+1;
	vector<double> gofr_count_mn(r_num, 0);
	vector<double> gofr_count_mm(r_num, 0);
	vector<double> gofr_count_nn(r_num, 0);
	int count_data = 0;
	for (int i_time = (int)(0.8*sys.i_time_max); true; i_time ++) {
		//if (sys.time_list[i_time] > 100 && sys.time_list[i_time] < 220)
		cerr << i_time << " / " << sys.i_time_max << endl;
		count_data ++;
		sys.i_time_current = i_time;
		//sys.set_positions(sys.i_time_current);
		sys.initializeBoxing();
		sys.in_window.clear();
		sys.in_window.resize(sys.np);
		for (int i=0; i<sys.np; i++) {
			sys.in_window[i] = sys.check_in_window(sys.position[i]);
			if (sys.in_window[i]) {
				Box* b = sys.boxset.WhichBox(sys.position[i]);
				vector<int> list_near = b->neighborhood_container;
				int n_neighobr = 0;
				for (const auto& j : list_near) {
					if (i != j) {
						vec3d vec = sys.position[j]-sys.position[i];
						if (sys.periodic_boundary) {
							vec.periodicBoundaryDiff(sys.lx, 0, sys.lz);
						}
						double r = vec.norm();
						if (r < r_max && r > r_min) {
							int i_r = (r-r_min) / dr;
							if (sys.ptype[i] != sys.ptype[j]) {
								gofr_count_mn[i_r] += 1/(2*M_PI*r*dr);
							} else if (sys.ptype[i] == 1 && sys.ptype[j] == 1) {
								gofr_count_mm[i_r] += 1/(2*M_PI*r*dr);
							} else if (sys.ptype[i] == -1 && sys.ptype[j] == -1) {
								gofr_count_nn[i_r] += 1/(2*M_PI*r*dr);
							}
						}
					}
				}
			}
		}
		
	}
	
	
	for (int i =0; i<r_num; i++){
		double r = r_min + dr*i;
		sys.fout << r << ' ';
		sys.fout << gofr_count_mn[i]*1.0/(sys.np*count_data) << ' ';
		sys.fout << gofr_count_mm[i]*1.0/(sys.np*count_data) << ' ';
		sys.fout << gofr_count_nn[i]*1.0/(sys.np*count_data) << endl;
		
	}
	
	sys.fout.close();
}




#endif /* PairCorrelationFunction_h */
