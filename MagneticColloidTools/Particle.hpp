//
//  Particle.hpp
//  gbanalyzer
//
//  Created by Ryohei Seto on 2015/10/06.
//  Copyright © 2015年 Ryohei Seto. All rights reserved.
//

#ifndef Particle_hpp
#define Particle_hpp

#include <stdio.h>
#include <vector>
#include <iostream>
#include "vec3d.h"

typedef std::pair<int, double> pair_t;

//bool compare_second(const pair_t &a, const pair_t &b) {
//	return (a.second < b.second);
//}

class Particle{

private:

public:
	Particle(int id_, const vec3d& pos, int ptype, double a, int i_time){
		id = id_;
		radius = a;
		particle_type = ptype;
		active = true;
		position.clear();
		time.clear();
		position.push_back(pos);
		time.push_back(i_time);
	};
	
	~Particle(){
		
	};

	void reuse(int id_, const vec3d& pos, int ptype, double a, int i_time){
		id = id_;
		radius = a;
		particle_type = ptype;
		active = true;
		position.clear();
		position.push_back(pos);
		time.clear();
		time.push_back(i_time);
	}
	
	void track_add (const vec3d& pos, int i_time,
					int id_particle, double distance) {
		position.push_back(pos);
		time.push_back(i_time);
		tracked_distance = distance;
		tracked_position_id = id_particle;
		active = true;
	}
	
	void trac_lost() {
		position.clear();
		time.clear();
		active = false;
	}
	
	int id;
	int number;
	int particle_type;
	double tracked_distance;
	double radius;
	int tracked_position_id;
	bool active;
	vec3d position_averaged;
	std::vector<vec3d> position;
	std::vector<int> time;
	
	void averaging_position(int average_num) {
		vec3d pos_average(0, 0, 0);
		int data_num = (int)position.size();
		int cnt = 0;
		if (data_num > 1) {
			for (int k=1; k<=average_num; k++) {
				if (k <= data_num) {
					pos_average += position[data_num-k];
					cnt++;
				}
			}
			pos_average = pos_average/cnt;
		} else {
			pos_average = position.back();
		}
		position_averaged = pos_average;
	}

	
//	void add_possibility(const int p_id, const vec3d& pos) {
//		pos_possible.push_back(pos);
//		p_id_possible.push_back(p_id);
//		if (pos_possible.size()==2){
//			exit(1);
//		}
//	}
	//std::vector <double> possibility_distance;
	//std::vector <int> possibility_p_id;
	
	void output() {
		for (auto pos : position) {
			std::cout << pos.x << ' ' << pos.z << std::endl;
		}
		std::cout << std::endl;
	}
	
};



#endif /* Particle_hpp */

