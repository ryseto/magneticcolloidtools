///////
//  System.hpp
//  gbanalyzer
//
//  Created by Ryohei Seto on 2015/10/05.
//  Copyright © 2015年 Ryohei Seto. All rights reserved.
//

#ifndef System_hpp
#define System_hpp

#include <stdio.h>
#include <vector>
#include <iostream>
#include <fstream>
#include <complex>
#include <string>
#include <sstream>
#include "vec3d.h"
#include "Particle.hpp"
#include "BoxSet.hpp"

using namespace std;

class System{
private:
	
protected:
public:
	System():
	cutoff_factor_max(1.5),
	cutoff_factor_min(0.8)
	{
		twodimension = true;
		periodic_boundary = false;
	};
	~System(){};
	int np; ///< nb of particles
	int np_mag;
	int np_nonmag;
	int np_range;
	BoxSet boxset;
	std::vector<vec3d> position;
	std::vector<double> radius;
	int i_time_current;
	int i_time_max;
	int largest_cluster;
	int num_average;
	double shear_strain;
	double correlation_threshold;
	const double cutoff_factor_max;
	const double cutoff_factor_min;
	
	vector<vector<int>> bond;
	vector<set<int>> bond_crystal;
	
	std::vector<int> ptype;
	std::vector<complex<double>> phi;
	std::vector<double> abs_phi;
	std::vector<double> arg_phi;
	std::vector<bool> in_cluster;

//	std::vector<std::vector <vec3d>> all_pos;
//	std::vector<std::vector <int>> all_ptype;
//	std::vector<std::vector <bool>> all_movable;
//	std::vector<std::vector <double>> all_radius;
	std::vector<std::vector<int>> voronoi_next;
	std::vector<std::vector<vec3d>> voronoi_vertex1;
	std::vector<std::vector<vec3d>> voronoi_vertex2;
	
	std::vector<int> particle_id;
	std::vector<Particle*> particles;
	std::vector<bool> in_window;
	std::vector<double> time_list;
	std::vector<set<int>> clusters;

	string data_name;
	bool periodic_boundary;
	bool twodimension;
	bool ptype_check; // Check particle type (mag or non-mag) for OP calculation
	bool draw_voronoi_cell;
	bool draw_square_cell;
	bool output_voronoi_data;
	bool output_displacement_data;
	bool normaloutput;
	bool mag_simulation;
	double x_min;
	double x_max;
	double z_min;
	double z_max;
	double lx;
	double ly;
	double lz;
	double lx_half;
	double lz_half;

	double box_size;
	double average_cluster_size;
	double average_cluster_size2;
	double number_of_particles_crystal;
	int largest_crystal;
	int nn_include_phi;
	double shear_disp;
	vector<vec3d> position_init;

	ofstream fout;
	ofstream fout_yap;
	ofstream fout_OPconfig;
	//ofstream fout_configdata;
	ofstream fout_ss;
	ofstream fout_bond;
	ofstream fout_GB;
	ofstream fout_orientation;
	ofstream fout_phihist;
	ofstream fout_OPvoronoi;
	ofstream fout_disp;
	
	void set_box(double x_min_,
				 double x_max_,
				 double z_min_,
				 double z_max_)
	{
		x_min = x_min_;
		x_max = x_max_;
		z_min = z_min_;
		z_max = z_max_;
		cerr << x_min << ' ' << x_max << endl;
		cerr << z_min << ' ' << z_max << endl;
	}
//
//	bool range(const vec3d& pos_)
//	{
//		if (periodic_boundary) {
//			return true;
//		}
//		if (pos_.x > x_min && pos_.x < x_max
//			&& pos_.z > z_min && pos_.z < z_max) {
//			return true;
//		}
//		return false;
//	}
	
	bool check_in_window(const vec3d& pos)
	{
		return true;
		if (periodic_boundary) {
			return true;
		}
		if (pos.x < x_min) {
			return false;
		} else if (pos.x > x_max) {
			return false;
		} else if (pos.z < z_min) {
			return false;
		} else if (pos.z > z_max) {
			return false;
		}
		return true;
	}
	
	double area_of_box()
	{
		return (x_max-x_min)*(z_max-z_min);
	}
	
	void initializeBoxing();
	
	
//	void set_positions(int i_time)
//	{
//		position.clear();
//		ptype.clear();
//		radius.clear();
//		np = (int)all_pos[i_time].size();
//		position.resize(np);
//		ptype.resize(np);
//		radius.resize(np);
//		for (int i=0; i<np; i++) {
//			position[i] = all_pos[i_time][i];
//			ptype[i] = all_ptype[i_time][i];
//			radius[i] = all_radius[i_time][i];
//		}
//	}
	
	void periodize(vec3d& pos);
	void periodicBoundaryDiffLE(vec3d& vec);

	
	void allocatePosition(){
		position.resize(np);
		abs_phi.resize(np);
		arg_phi.resize(np);
	}
	
	double get_lx()
	{
		return lx;
	}
	
	double get_ly()
	{
		return ly;
	}
	
	double get_lz()
	{
		return lz;
	}
	
	inline void set_np(int val)
	{
		np = val;
	}
	
	inline int get_np()
	{
		return np;
	}

	//void importData(ifstream& fin);
	int importData(ifstream& input_data);

	int importData_exdata(ifstream& input_data);

	int importDataShear(ifstream& input_data);

	
	void prepareSimulationName(const string filename_import)
	{
		/**
	  \brief Determine simulation name.
		 */
		ostringstream ss_simu_name;
		string::size_type name_start;
		if (filename_import.find("config_") == 0) {
			name_start = filename_import.find("config_")+7;
		} else {
			name_start = 0;
		}
		string::size_type name_end = filename_import.find_last_of(".");
		ss_simu_name << filename_import.substr(name_start, name_end-name_start);
		data_name = ss_simu_name.str();
		
		cerr << data_name << endl;
		//exit(1);
	}
};
#endif /* System_hpp */







