//
//  GBdomain.h
//  gbanalyzer
//
//  Created by Ryohei Seto on 2015/10/07.
//  Copyright © 2015年 Ryohei Seto. All rights reserved.
//

#ifndef GBdomain_h
#define GBdomain_h
#include <vector>
#include <set>
#include "System.hpp"
#include "vec3d.h"
#include "BoxSet.hpp"
#include "Box.hpp"
#include "Particle.hpp"
using namespace std;

const double min_phi = 0.8;
const double min_ordiff = 0.02;

void importData(System& sys,
				vector<vec3d>& positions,
				vector<double>& abs_phi,
				vector<double>& arg_phi);

bool correlatingBond(int i, int j, System& sys)
{
	bool different_kind = true;
	if (i < sys.np/2 && j < sys.np/2){
		different_kind = false;
	} if (i >= sys.np/2 && j >= sys.np/2){
		different_kind = false;
	}
	if (different_kind) {
		vec3d dr = sys.position[i]-sys.position[j];
		dr.periodicBoundaryDiff(sys.lx, sys.ly, sys.lz);
		if (dr.norm() < 2.3) {
			double ang_diff = abs(sys.arg_phi[i]-sys.arg_phi[j]);
			if (ang_diff > 0.5) {
				ang_diff = 1-ang_diff;
			}
			if (ang_diff < min_ordiff) {
				if (sys.abs_phi[i] > min_phi && sys.abs_phi[j] > min_phi) {
					return true;
				}
			}
		}
	}
	return false;
}

void investigate(System& sys,
				 vector<set<int>>& clusters,
				 vector<int>& to_be_checked,
				 vector<bool>& checked,
				 int& cluster_id,
				 vector<vector<int>>& neighbors
				 )
{
	int i = to_be_checked.back();
	to_be_checked.pop_back();
	if (checked[i] == false) {
		int cb = 0;
		checked[i] = true;
		clusters[cluster_id].insert(i);
		for (auto nn : neighbors[i]) {
			if (correlatingBond(i, nn, sys)){
				cb++;
			}
		}
		for (auto nn : neighbors[i]) {
			if (checked[nn] == false) {
				to_be_checked.push_back(nn);
			}
		}
		
	}
}


void search_clusters(System& sys,
					 vector<vector<int>>& neighbors,
					 vector<set<int>>& clusters)
{
	vector<bool> checked(sys.np, false);
	int cluster_id = 0;
	vector<int> to_be_checked;
	for (int i=0; i< sys.np; i++) {
		if (checked[i] == false) {
			set<int> a;
			clusters.push_back(a);
			to_be_checked.push_back(i);
			while (!to_be_checked.empty()) {
				investigate(sys, clusters, to_be_checked, checked, cluster_id, neighbors);
			}
			cluster_id++;
		}
	}
}

void gbdomain()
{
	System sys;
	sys.periodic_boundary = true;
	vector <vec3d> position;
	vector <double> abs_phi;
	vector <double> arg_phi;
	importData(sys, position, abs_phi, arg_phi);
	sys.np = (int)position.size();
	sys.allocatePosition();
	for (int i=0; i< sys.np; i++) {
		sys.position[i] = position[i];
		sys.abs_phi[i] = abs_phi[i];
		sys.arg_phi[i] = arg_phi[i];
	}
	BoxSet boxset;
	boxset.init(2.2, &sys);
	for (int i=0; i<sys.np; i++) {
		boxset.box(i);
	}
	boxset.update();
	vector<vector<int>> neighbors(sys.np);
	for (int i=0; i< sys.np; i++) {
		vec3d pos = position[i];
		Box *b = boxset.WhichBox(pos);
		vector<int> list_near_in_previous = b->neighborhood_container;
		for (const auto& j : list_near_in_previous) {
			if (correlatingBond(i, j, sys)){
				neighbors[i].push_back(j);
				cerr << i << "==>" << j << endl;
			}
		}
		//p->add_possibility(nearest_particle, sys.position[nearest_particle]);
		//cerr << min_sq_distance << endl;
	}

	vector<set<int>> clusters;
	search_clusters(sys, neighbors, clusters);
	
	cout << "@0 0 0 0 \n";
	cout << "@1 255 255 255 \n";
	cout << "@2 200 200 200 \n";
	cout << "@3 250 50 0\n";
	cout << "@14 250 100 0\n";
	cout << "@5 250 150 0\n";
	cout << "@16 250 200 0\n";
	cout << "@7 250 250 0\n";
	cout << "@18 200 250 0\n";
	cout << "@9 150 250 0\n";
	cout << "@20 100 250 0\n";
	cout << "@11 50 250 0\n";
	cout << "@12 0 250 0\n";
	cout << "@13 0 250 50\n";
	cout << "@4 0 250 100\n";
	cout << "@15 0 250 150\n";
	cout << "@6 0 250 200\n";
	cout << "@17 0 250 250\n";
	cout << "@8 0 200 250\n";
	cout << "@19 0 150 250\n";
	cout << "@10 0 100 250\n";
	cout << "@21 0  50 250\n";
	cout << "@22 0  0  250\n";
	cout << "@23 50  0 250\n";
	cout << "@24 100 0 250\n";
	cout << "@25 150 0 250\n";
	cout << "@26 200 0 250\n";
	cout << "@27 250 0 250\n";
	cout << "@28 250 0 200 \n";
	cout << "@29 250 0 150 \n";
	cout << "@30 250 0 100 \n";
	cout << "@31 250 0 50 \n";
	cout << "@32 250 0 0 \n";

	int color = 3;
	for (auto c : clusters) {
		if (c.size() <=1 ) {
			cout << "@ " << 1 << endl;
		} else {
			if (color == 33) {
				color = 3;
			}
			
			cout << "@ " << color++ << endl;
		}
		
		for (auto i : c) {
			cout << "c " << position[i].x  << " 0 " << position[i].z << endl;
		}
	}
	cout << "@ " << 0 << endl;
	cout << "r " << 0.2 << endl;
	int i_max = (int)neighbors.size();
	for (int i=0; i <i_max; i++) {
		for (auto nn : neighbors[i]) {
			if ((position[i]-position[nn]).norm() < 100) {
				cout << "s " << position[i].x << " -0.1 " << position[i].z << ' ';
				cout << position[nn].x << " -0.1 " << position[nn].z << endl;
			} else {
				vec3d dbond = position[nn]- position[i];
				dbond.periodicBoundaryDiff(sys.lx, sys.ly, sys.lz);
				cout << "s " << position[i].x << " -0.1 " << position[i].z << ' ';
				cout << position[i].x + dbond.x << " -0.1 " << position[i].z + dbond.z << endl;
			}
		}
	}
	
}

void importData(System& sys,
				vector <vec3d>& positions,
				vector <double>& abs_phi,
				vector <double>& arg_phi)
{
	ifstream input_data;
	input_data.open("simu.dat");
	double lx_;
	input_data >> lx_;
	sys.lx = lx_;
	sys.ly = 0;
	sys.lz = lx_;
	string buf[4];
	while (1) {
		input_data >> buf[0] >> buf[1] >> buf[2] >> buf[3];
		vec3d pos(atof(buf[0].c_str()), 0, atof(buf[1].c_str()));
		double abs_phi_p = atof(buf[2].c_str());
		double arg_phi_p = (atof(buf[3].c_str()) + M_PI/4)/ (M_PI/2);
		positions.push_back(pos);
		abs_phi.push_back(abs_phi_p);
		arg_phi.push_back(arg_phi_p);
		//cerr << arg_phi4.back() << endl;
		if (input_data.eof()) {
			break;
		}
	}
	input_data.close();
}

#endif /* GBdomain_h */



