#!/bin/bash
#SBATCH --job-name=convert
#SBATCH --partition=compute
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --mem=1G
#SBATCH --mail-type=FAIL,END
#SBATCH --array=1-40
export OMP_NUM_THREADS=${SLURM_CPUS_PER_TASK}
export CHOLMOD_OMP_NUM_THREADS=${SLURM_CPUS_PER_TASK}
cd $SLURM_SUBMIT_DIR

let k=($SLURM_ARRAY_TASK_ID-1)%10+1
let kk=($SLURM_ARRAY_TASK_ID-1)/10

parameterfile=(magbinary magfixedparticles1 magfixedparticles2 magfixedparticles3)
pf=${parameterfile[$kk]}
averageConfigdata.pl par_D2N2000VF0.68Bidi1_nr0.5Square_${k}_${pf}_magnetic15b.dat 1 1 27.9715  \
&& MagneticColloidTools -o config_D2N2000VF0.68Bidi1_nr0.5Square_${k}_${pf}_magnetic15b.dat -p -y cluster