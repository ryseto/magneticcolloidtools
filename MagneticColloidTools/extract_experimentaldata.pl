#!/usr/bin/perl

# Usage:
# $ averageConfigdata.pl par_[...].dat [force_factor] [y_section]
#
# force_factor: To change the widths of strings to exhibit forces.
# y_section: Visualize the trimed range of the y coordinates.

use Math::Trig;
use IO::Handle;
$exdataInput = $ARGV[0];

$j = index($exdataInput ,'.orig', $i-1);
$exdataOutput = substr($exdataInput, 0, $j);

printf "$exdataOutput\n";

open (INEXDATA, "< ${exdataInput}");
open (OUT, "> ${exdataOutput}");

printf "$exdataOutput\n";
printf "$exdataInput\n";

while (1){
	$line = <INEXDATA>;
	last unless defined $line;
	printf OUT "$line";
	
	$line = <INEXDATA>;
	printf OUT "$line";
	($tmp, $np) = split(/\s+/, $line);
	printf "$np\n";
	for ($i=0; $i < $np; $i++) {
		$line = <INEXDATA>;
		($x, $y, $d1, $d2, $d3) = split(/\s+/, $line);
		printf OUT "$x $y\n";
	}
	$line = <INEXDATA>;
	printf OUT "$line";
	($tmp, $np) = split(/\s+/, $line);
	printf "$np\n";
	for ($i=0; $i < $np; $i++) {
		$line = <INEXDATA>;
		($x, $y, $d1, $d2, $d3) = split(/\s+/, $line);
		printf OUT "$x $y\n";
	}
	
}

close (OUT);
close (IN);


