//
//  OrderParameter.h
//  gbanalyzer
//
//  Created by Ryohei Seto on 2015/10/11.
//  Copyright © 2015年 Ryohei Seto. All rights reserved.
//

#ifndef OrderParameter_h
#define OrderParameter_h
#include <vector>
#include <set>
#include <complex>
#include "System.hpp"
#include "vec3d.h"
#include "BoxSet.hpp"
#include "Box.hpp"
#include "Particle.hpp"
#include "Yaplot.h"
using namespace std;
void estimateSystemSize(System& sys);
void calcPhi4(System& sys);
void calcPhi6(System& sys);
void calcPhi4CorrelationBond(System& sys);
void findCrystalDomain(System& sys);
void calcAveragePhi4(System& sys);
void outputPhi4Config(System& sys);
void outputYaplot_OP(System& sys, int yaplot_order_parameter);
void outputYaplot_cluster(System& sys);
void outputYaplot_glassy(System& sys);
void outputYaplot_voronoi(System& sys);
void construct_crystal_cluster(System& sys);
void find_crystal_bond(System& sys);
void histogramOrientation(System& sys);
void histogramOrderParameter(System& sys);
void voronoi(System& sys);
void magneticAnalysys(System &sys, int yaplot_order_parameter, int i_opt);
void shearAnalysys(System &sys, int yaplot_order_parameter);

void OrderParameter(string file_name,
					bool ptype_check,
					double bond_order_trheshold,
					int yaplot_order_parameter,
					bool draw_voronoi_cell,
					bool draw_square_cell,
					double box_size,
					double nn_include_phi)
{
	System sys;
	sys.output_voronoi_data = true;
	sys.output_displacement_data = false;
	sys.normaloutput = true;
	sys.mag_simulation = false;
	double output_time_list[] = {300}; // when normaloutput = false.
	
	sys.ptype_check = ptype_check;
	sys.draw_voronoi_cell = draw_voronoi_cell;
	sys.draw_square_cell = draw_square_cell;
	sys.correlation_threshold = bond_order_trheshold;
	sys.box_size = box_size;
	sys.nn_include_phi = nn_include_phi;
	
	std::ifstream input_data;
	bool simulation_data;
	input_data.open(file_name.c_str());
	if (sys.mag_simulation) {
		if (file_name.find("ex") == 0) {
			cerr << "experimental data" << endl;
			simulation_data = false;
			sys.periodic_boundary = false;
			sys.lx = 0;
			sys.ly = 0;
			sys.lz = 0;
		} else {
			cerr << "simulation data" << endl;
			simulation_data = true;
			std::string buf[6];
			int cnt = 0;
			input_data >> buf[0] >> buf[1] >> buf[2] >> buf[3] >> buf[4] >> buf[5];
			sys.lx = atof(buf[1].c_str());
			sys.ly = 0;
			sys.lz = atof(buf[3].c_str());
			sys.lx_half = sys.lx/2;
			sys.ly = 0;
			sys.lz_half = sys.lz/2;
			sys.periodic_boundary = atoi(buf[5].c_str());
			cerr << "periodic_boundary = " << sys.periodic_boundary << endl;
		}
	} else {
		std::string buf[6];
		input_data >> buf[0] >> buf[1] >> buf[2] >> buf[3] >> buf[4] >> buf[5];
		sys.lx = atof(buf[1].c_str());
		sys.ly = 0;
		sys.lz = atof(buf[3].c_str());
		sys.lx_half = sys.lx/2;
		sys.lz_half = sys.lz/2;
		sys.periodic_boundary = atoi(buf[5].c_str());
		
	}
	sys.prepareSimulationName(file_name);
	if (!input_data){
		cerr << "not found" << endl;
		exit(1);
	}
	stringstream bo_trheshold_ss;
	bo_trheshold_ss << "_" << bond_order_trheshold;
	if (sys.mag_simulation) {
		string order_parameter_output = "OP_"+sys.data_name+bo_trheshold_ss.str()+".dat";
		sys.fout.open(order_parameter_output);
		string order_parameter_config = "OP_data"+sys.data_name+bo_trheshold_ss.str()+".dat";
		
		string order_parameter_snapshot = "Snapshot"+sys.data_name+bo_trheshold_ss.str()+".dat";
		sys.fout_ss.open(order_parameter_snapshot);
		string order_parameter_bond = "Bond"+sys.data_name+bo_trheshold_ss.str()+".dat";
		sys.fout_bond.open(order_parameter_bond);
		
		string orientation_output = "ori_"+sys.data_name+bo_trheshold_ss.str()+".dat";
		sys.fout_orientation.open(orientation_output);
		
		string op_voronoi = "OP_voronoi_"+sys.data_name+bo_trheshold_ss.str()+".dat";
		if (sys.output_voronoi_data) {
			sys.fout_OPvoronoi.open(op_voronoi);
		}
		
		string disp_data = "Disp_"+sys.data_name+bo_trheshold_ss.str()+".dat";
		if (sys.output_displacement_data) {
			sys.fout_disp.open(disp_data);
			
		}
		string phihist_output = "hist_"+sys.data_name+bo_trheshold_ss.str()+".dat";
		sys.fout_phihist.open(phihist_output);
		if (yaplot_order_parameter == 3) {
			string GBplotfile = "GB_"+sys.data_name+bo_trheshold_ss.str()+".yap";
			sys.fout_GB.open(GBplotfile);
		} else {
			string order_parameter_yaplot = "OP_"+sys.data_name+bo_trheshold_ss.str()+".yap";
			if (yaplot_order_parameter >= 0) {
				sys.fout_yap.open(order_parameter_yaplot);
			}
			//		string order_parameter_config = "OPconfig_"+sys.data_name+".dat";
			//		sys.fout_OPconfig.open(order_parameter_config);
		}
		
		if (yaplot_order_parameter == 0) {
			outputYapColorRainbow(sys.fout_yap);
		} else if (yaplot_order_parameter == 1) {
			outputYapColorHue(sys.fout_yap);
		} else if (yaplot_order_parameter == 2) {
			outputYapColorHue(sys.fout_yap);
		} else if (yaplot_order_parameter == 3) {
			outputYapColorHue(sys.fout_GB);
		} else if (yaplot_order_parameter == 4) {
			//outputYapColorHue(sys.fout_yap);
			outputYapColorRainbow(sys.fout_yap);
		}
	} else {
		string order_parameter_output = "OP_"+sys.data_name+".dat";
		sys.fout.open(order_parameter_output);

		string order_parameter_yaplot = "OP_"+sys.data_name+".yap";
		sys.fout_yap.open(order_parameter_yaplot);

		if (yaplot_order_parameter == 0) {
			outputYapColorRainbow(sys.fout_yap);
		} else if (yaplot_order_parameter == 1) {
			outputYapColorHue(sys.fout_yap);
		}
	}
	
	//outputYapColorRainbow_cout();
	//sys.fout_OPconfig << sys.lx << ' ' << sys.lz << endl;
	//sys.i_time_max = (int)sys.all_pos.size();
	//double time_next = 20;
	
	
	int i_opt = 0;
	for (int i_time = 0; true; i_time ++) {
		cerr << i_time << endl;
		if (sys.mag_simulation) {
			if (simulation_data) {
				if (sys.importData(input_data)) {
					cerr << "End of Data." << endl;
					return;
				}
			} else {
				if (sys.importData_exdata(input_data)) {
					cerr << "End of Data." << endl;
					return;
				}
			}
		} else {
			cerr << "i_time " << i_time << endl;
			sys.importDataShear(input_data);
			
		}
		if (i_time == 0) {
			estimateSystemSize(sys);
			sys.position_init.resize(sys.np);
			for (int i=0; i<sys.np; i++) {
				sys.position_init[i] = sys.position[i];
			}
		}
		sys.i_time_current = i_time;
		if (0) {
			if (sys.normaloutput == true ||
				sys.time_list[sys.i_time_current] > output_time_list[i_opt]){
				if (sys.mag_simulation) {
					magneticAnalysys(sys, yaplot_order_parameter, i_opt);
				} else {
					shearAnalysys(sys, yaplot_order_parameter);
					cerr << "				shearAnalysys(sys, yaplot_order_parameter); " << endl;
					exit(1);
				}
			}
		} else {
			shearAnalysys(sys, yaplot_order_parameter);
		}
		
		//}
		//		if (sys.time_list[sys.i_time_current] > time_next) {
		//			histogramOrientation(sys);
		//			histogramOrderParameter(sys);
		//			time_next += 100;
		//		}
		//		if (sys.time_list[sys.i_time_current] > 225){
		//			exit(1);
		//		}
	}
	
	//	histogramOrientation(sys);
	//	histogramOrderParameter(sys);
	input_data.close();
	
	sys.fout.close();
	sys.fout_GB.close();
	sys.fout_OPconfig.close();
	sys.fout_yap.close();
	sys.fout_orientation.close();
	sys.fout_phihist.close();
	sys.fout_ss.close();
}

void calcAverageOrientation(System &sys)
{
	static double previous_mean_arg_phi = 0;
	double sum_arg_phi = 0;
	double sum_sq_arg_phi = 0;
	double sum_abs_phi = 0;
	
	for (int i=0; i<sys.np; i++) {
		double arg_phi = arg(sys.phi[i])/6;
		while (arg_phi > previous_mean_arg_phi+M_PI/6) {
			arg_phi -= M_PI/3;
		}
		while (arg_phi < previous_mean_arg_phi-M_PI/6) {
			arg_phi += M_PI/3;
		}
		if (abs(arg_phi - previous_mean_arg_phi) > M_PI/6	) {
			cout << arg_phi << ' ' << previous_mean_arg_phi << endl;
			exit(1);
		}
		sum_abs_phi += abs(sys.phi[i]);
		sum_arg_phi += arg_phi;
		sum_sq_arg_phi += arg_phi*arg_phi;
	}
	
	double mean_arg_phi = sum_arg_phi/sys.np;
	double stddev_arg_phi = sqrt(sum_sq_arg_phi/sys.np-mean_arg_phi*mean_arg_phi);
	double mean_abs_phi = sum_abs_phi/sys.np;
	sys.fout << sys.shear_strain << ' ' << mean_arg_phi << ' ' << stddev_arg_phi << ' ' << mean_abs_phi << endl;
	
	previous_mean_arg_phi = mean_arg_phi;
	
}

void shearAnalysys(System &sys, int yaplot_order_parameter)
{
	sys.initializeBoxing();
	sys.voronoi_next.clear();
	sys.voronoi_next.resize(sys.np);
	sys.voronoi_vertex1.clear();
	sys.voronoi_vertex1.resize(sys.np);
	sys.voronoi_vertex2.clear();
	sys.voronoi_vertex2.resize(sys.np);
	sys.in_window.clear();
	sys.in_window.resize(sys.np);
	voronoi(sys);
	calcPhi6(sys);
	sys.boxset.clear();
	outputYaplot_OP(sys, yaplot_order_parameter);
	//calcAverageOrientation(sys);
	cerr << "complete\n" ;
}

void magneticAnalysys(System &sys, int yaplot_order_parameter, int i_opt)
{
	cerr << "outtime = " << sys.time_list[sys.i_time_current] << endl;
	//		sys.set_positions(sys.i_time_current);
	sys.initializeBoxing();
	sys.voronoi_next.clear();
	sys.voronoi_next.resize(sys.np);
	sys.voronoi_vertex1.clear();
	sys.voronoi_vertex1.resize(sys.np);
	sys.voronoi_vertex2.clear();
	sys.voronoi_vertex2.resize(sys.np);
	sys.in_window.clear();
	sys.in_window.resize(sys.np);
	voronoi(sys);
	calcPhi4(sys);
	calcPhi4CorrelationBond(sys);
	findCrystalDomain(sys);
	sys.boxset.clear();
	calcAveragePhi4(sys);
	//	outputPhi4Config(sys);
	if (yaplot_order_parameter == 0
		|| yaplot_order_parameter == 1) {
		outputYaplot_OP(sys, yaplot_order_parameter);
	} else if (yaplot_order_parameter == 2) {
		outputYaplot_cluster(sys);
	} else if (yaplot_order_parameter == 3) {
		outputYaplot_glassy(sys);
	} else if (yaplot_order_parameter == 4) {
		outputYaplot_voronoi(sys);
	}
	int count_np = 0;
	for (int i=0; i<sys.np; i++) {
		if (sys.voronoi_next[i].size() > 0) {
			count_np ++;
		}
	}
	if (sys.output_voronoi_data) {
		sys.fout_OPvoronoi << "np " << count_np << endl;
		for (int i=0; i<sys.np; i++) {
			if (sys.voronoi_next[i].size() > 0) {
				if (sys.in_cluster[i] == false) {
					sys.fout_OPvoronoi << -1 << ' ';
				} else {
					double op = arg(sys.phi[i])/(2*M_PI)+0.5;
					sys.fout_OPvoronoi << op << ' ';
				}
				for (int j=0; j<sys.voronoi_next[i].size(); j++) {
					sys.fout_OPvoronoi << sys.voronoi_vertex1[i][j].x << ' ' << sys.voronoi_vertex1[i][j].z << ' ' ;
				}
				sys.fout_OPvoronoi << endl;
			}
		}
		sys.fout_OPconfig.close();
		//exit(1);
	}
	if (sys.output_displacement_data) {
		sys.fout_disp << "np " << count_np << endl;
		for (int i=0; i<sys.np; i++) {
			vec3d disp = sys.position[i]-sys.position_init[i];
			//disp.periodicBoundaryDiff(sys.lx, 0, sys.lz);
			sys.fout_disp << sys.position[i].x << ' ' << sys.position[i].z << ' ' << disp.x << ' ' << disp.z << ' ' << disp.norm() << endl;
		}
		sys.fout_disp.close();
		exit(1);
	}
	i_opt++;
	if (sys.normaloutput == false) {
		for (int i=0; i<sys.np; i++) {
			sys.fout_ss << sys.position[i].x << ' ' << sys.position[i].z << ' ' << abs(sys.phi[i]) << endl;
		}
		sys.fout_ss << endl;
		//sys.fout_bond
		
		if (sys.draw_square_cell) {
			for (int i=0; i<sys.np; i++) {
				for (const auto j : sys.bond[i]) {
					bool crystal = false;
					for (const auto jc : sys.bond_crystal[i]) {
						if (j == jc) {
							crystal = true;
							break;
						}
					}
					vec3d diff_vec = sys.position[i]-sys.position[j];
					if (diff_vec.norm() < 10) {
						sys.fout_bond << sys.position[i].x << ' ' << sys.position[i].z << ' ';
						sys.fout_bond << sys.position[j].x << ' ' << sys.position[j].z << ' ';
						if (crystal) {
							sys.fout_bond << 1 << endl;
						} else {
							sys.fout_bond << 0 << endl;
						}
					}
				}
			}
		}
	}
}


void estimateSystemSize(System& sys)
{
	if (sys.periodic_boundary == false) {
		double margine = 0;
		double x_min = 1000;
		double x_max = -100;
		double z_min = 1000;
		double z_max = -100;
		int num_particle = (int)sys.position.size();
		for (int i=0; i<num_particle; i++) {
			if (sys.position[i].x < x_min) {
				x_min = sys.position[i].x;
			}
			if (sys.position[i].x > x_max) {
				x_max = sys.position[i].x;
			}
			if (sys.position[i].z < z_min) {
				z_min = sys.position[i].z;
			}
			if (sys.position[i].z > z_max) {
				z_max = sys.position[i].z;
			}
		}
		sys.set_box(x_min+margine, x_max-margine, z_min+margine, z_max-margine);
	}
}

void voronoiFindVertex(System& sys, int i,
					   vector<int>& voronoi_next_particle,
					   vector<vec3d>& voronoi_vertex1,
					   vector<vec3d>& voronoi_vertex2)
{
	vec3d p = sys.position[i];
	Box* b = sys.boxset.WhichBox(sys.position[i]);
	vector<int> list_near = b->neighborhood_container;
	int number_of_near = (int)list_near.size();
	vector<bool> voronoi_next(number_of_near);
	vector<double> coeff_a(number_of_near);
	vector<double> coeff_b(number_of_near);
	vector<double> const_c(number_of_near);
	vector<vec3d> p_next(number_of_near);
	for (int j=0; j<number_of_near; j++) {
		int i_next = list_near[j];
		p_next[j] = sys.position[i_next];
//		sys.periodize(p_next[j]);
		vec3d p_diff = p_next[j]-p;
//		sys.periodicBoundaryDiffLE(p_diff);
		p_next[j] = p + p_diff;
		vec3d p_mid = 0.5*(p+p_next[j]);

		coeff_a[j] = p_diff.x; //p_next[j].x-p.x;
		coeff_b[j] = p_diff.z; // p_next[j].z-p.z;
		const_c[j] = p_mid.x*coeff_a[j]+p_mid.z*coeff_b[j];
	}
	const double initial_evaluation_value = 10000;
	for (int j=0; j<number_of_near; j++) {
		int i_next = list_near[j];
		bool voronoi_next_j = true;
		double check_side1 = initial_evaluation_value;
		double check_side2 = -initial_evaluation_value;
		vec3d vertex_plus;
		vec3d vertex_minus;
		for (int jj=0; jj<number_of_near; jj++) {
			if (j != jj) {
				int i_check = list_near[jj];
				vec3d p_check = sys.position[i_check];
				vec3d p_diff = p_check - p;
				//sys.periodicBoundaryDiffLE(p_diff);
//				sys.periodize(p_check);
				//@@@@@@@@@@@@@
//				if (abs(p_check.z-p.z) > sys.lz/2) {
//					if (p_check.z > p.z) {
//						p_check.z -= sys.lz;
//					} else {
//						p_check.z += sys.lz;
//					}
//				}
//				if (abs(p_check.x-p.x) > sys.lx/2) {
//					if (p_check.x > p.x) {
//						p_check.x -= sys.lx;
//					} else {
//						p_check.x += sys.lx;
//					}
//				}

				vec3d p_cross;
				double det = coeff_a[j]*coeff_b[jj]-coeff_b[j]*coeff_a[jj];
				if (det != 0) {
					p_cross.x = (const_c[j]*coeff_b[jj]-const_c[jj]*coeff_b[j])/det;
					p_cross.y = 0;
					p_cross.z = (-const_c[j]*coeff_a[jj]+const_c[jj]*coeff_a[j])/det;
					double side_cross = (p_next[j].z-p.z)*(p_cross.x-p.x)-(p_next[j].x-p.x)*(p_cross.z-p.z);
					double side_check = (p_next[j].z-p.z)*p_diff.x-(p_next[j].x-p.x)*p_diff.z;
					if (side_check > 0) {
						if (side_cross < check_side1) {
							check_side1 = side_cross;
							vertex_plus = p_cross;
						}
					} else {
						if (side_cross > check_side2) {
							check_side2 = side_cross;
							vertex_minus = p_cross;
						}
					}
					if (check_side2 > check_side1 ){
						voronoi_next_j = false;
						break;
					}
				}
			}
		}
		if (voronoi_next_j) {
			if (check_side1 != initial_evaluation_value
				&& check_side2 != -initial_evaluation_value) {
				double dist_plus = (vertex_plus-p).norm();
				double dist_minus = (vertex_minus-p).norm();
				if (dist_plus < 10 && dist_minus) {
					voronoi_next_particle.push_back(i_next);
					voronoi_vertex1.push_back(vertex_plus);
					voronoi_vertex2.push_back(vertex_minus);
				}
			}
		}
	}
}

void voronoi(System& sys)
{
	for (int i=0; i<sys.np; i++) {
		if (sys.periodic_boundary == false) {
			sys.in_window[i] = sys.check_in_window(sys.position[i]);
			sys.in_window[i] = true;
		}
		if (sys.periodic_boundary || sys.in_window[i]) {
			vector<int> voronoi_next_particle;
			vector<vec3d> voronoi_vertex1;
			vector<vec3d> voronoi_vertex2;
			voronoiFindVertex(sys, i, voronoi_next_particle,
							  voronoi_vertex1,
							  voronoi_vertex2);
			if (voronoi_next_particle.size() >= 2) {
				vec3d p1 = voronoi_vertex1[0];
				vec3d p_counter = voronoi_vertex2[0];
				int vn = voronoi_next_particle[0];
				set<int> klist;
				for (int k=1; k<voronoi_vertex1.size(); k++) {
					klist.insert(k);
				}
				int cnt = 0;
				while (true) {
					sys.voronoi_vertex1[i].push_back(p1);
					sys.voronoi_vertex2[i].push_back(p_counter);
					sys.voronoi_next[i].push_back(vn);
					if (klist.size() == 0) {
						break;
					}
					int k_erace;
					for (const int k : klist) {
						if (p_counter == voronoi_vertex1[k]) {
							p1 = p_counter;
							p_counter = voronoi_vertex2[k];
							vn = voronoi_next_particle[k];
							k_erace = k;
							break;
						} else if (p_counter == voronoi_vertex2[k]) {
							p1 = p_counter;
							p_counter = voronoi_vertex1[k];
							vn = voronoi_next_particle[k];
							k_erace = k;
							break;
						}
					}
					klist.erase(k_erace);
					if (cnt ++ > 100) {
						break;
					}
				}
			} else {

				sys.voronoi_vertex1[i].clear();
				sys.voronoi_next[i].clear();
			}
		}
	}
}

void findCrystalDomain(System& sys)
{
	find_crystal_bond(sys);
	//cout_yaplot(sys);
	construct_crystal_cluster(sys);
	cerr << "clusters " << sys.clusters.size() << endl;
	int sum_cluster_size = 0;
	int sum_cluster_size_sq = 0;
	int largest_crystal_tmp = 0;
	int i_largest = -1;
	//	for (const auto cluster: sys.clusters)
	for (int i=0; i<sys.clusters.size(); i++) {
		int cluster_size = sys.clusters[i].size();
		sum_cluster_size += cluster_size;
		sum_cluster_size_sq += cluster_size*cluster_size;
		if (cluster_size > largest_crystal_tmp) {
			largest_crystal_tmp = cluster_size;
			i_largest = i;
		}
	}
	
	sys.number_of_particles_crystal = sum_cluster_size;
	sys.largest_crystal = largest_crystal_tmp;
	sys.largest_cluster = i_largest;
	sys.average_cluster_size = (sum_cluster_size*1.0)/sys.clusters.size();
	sys.average_cluster_size2 = (sum_cluster_size_sq*1.0)/sum_cluster_size;
	if (sys.clusters.size() > 0) {
		cerr << sys.average_cluster_size << " ";
		cerr << sys.average_cluster_size2 << endl;
	} else {
		cerr << "no cluster" << endl;
	}
	
}

void search_cluster(System& sys, int k,
					vector<int>& check_list,
					set<int>& cluster,
					vector<bool>& checked)
{
	cluster.insert(k);
	checked[k] = true;
	for (const auto j: sys.bond_crystal[k]) {
		bool member = false;
		for (const auto p : cluster) {
			if (p == j) {
				member = true;
			}
		}
		if (member == false) {
			check_list.push_back(j);
		}
	}
}

bool check_square_crystal(System& sys,
						  int j, int i,
						  int& jn_crystal, int& in_crystal)
{
	for (const auto i_next : sys.bond[i]) {
		if (i_next != j) {
			for (const auto i_next_next : sys.bond[i_next]) {
				for (const auto j_next : sys.bond[j]) {
					if (j_next != i) {
						if (j_next == i_next_next) {
							in_crystal = i_next;
							jn_crystal = j_next;
							return true;
						}
					}
				}
			}
		}
	}
	return false;
}

void find_crystal_bond(System& sys)
{
	sys.bond_crystal.clear();
	sys.bond_crystal.resize(sys.np);
	int i = 0;
	while (i < sys.np) {
		for (const auto j : sys.bond[i]){
			if (j > i) {
				bool not_bonding = true;
				for (const auto ii: sys.bond_crystal[i]) {
					if (ii == j) {
						not_bonding = false;
						break;
					}
				}
				if (not_bonding) {
					int i_next, j_next;
					if (check_square_crystal(sys, j, i, j_next, i_next)) {
						/*
						 *   i ------ j
						 *   |        |
						 *   |        |
						 * i_next--j_next
						 */
						//						cerr << j << ' ' << i << ' ' <<  j_next << ' '<<  i_next << endl;
						//						cerr << endl;
						//						sys.position[i].cerr();
						//						sys.position[j].cerr();
						//						sys.position[i_next].cerr();
						//						sys.position[j_next].cerr();
						sys.bond_crystal[i].insert(j);
						sys.bond_crystal[j].insert(i);
						//
						sys.bond_crystal[i].insert(i_next);
						sys.bond_crystal[i_next].insert(i);
						//
						sys.bond_crystal[j].insert(j_next);
						sys.bond_crystal[j_next].insert(j);
						//
						sys.bond_crystal[j_next].insert(i_next);
						sys.bond_crystal[i_next].insert(j_next);
					}
				}
			}
		}
		i++;
	}
}

void construct_crystal_cluster(System& sys)
{
	sys.clusters.clear();
	int i = 0;
	vector<bool> checked(sys.np, false);
	while (i < sys.np) {
		if (checked[i] == false) {
			set<int> cluster;
			vector<int> check_list;
			check_list.push_back(i);
			checked[i] = true;
			while (!check_list.empty()) {
				int k = check_list.back();
				check_list.pop_back();
				search_cluster(sys, k, check_list, cluster, checked);
			}
			if (cluster.size() > 1) {
				sys.clusters.push_back(cluster);
			}
		}
		i++;
	}
	
	sys.in_cluster.resize(sys.np);
	for (auto ic : sys.in_cluster) {
		ic = false;
	}
	for (const auto cluster: sys.clusters) {
		for (const auto pp: cluster) {
			sys.in_cluster[pp] = true;
		}
	}
}

void histogramOrientation(System& sys)
{
	int cnt = 0;
	for (const auto cluster: sys.clusters) {
		vector<int> histogram_bin(100, 0);
		
		//op = angle_cluster[c++];
		//		int color = (int)(120*op);
		//		sys.fout_yap << "@ " << color+3 << endl;
		for (const auto pp: cluster) {
			double op = arg(sys.phi[pp])/(2*M_PI)+0.5;
			int i_orientation = (int)(op*100);
			histogram_bin[i_orientation] ++;
			cnt ++;
		}
		for (int i=0; i<100; i++) {
			sys.fout_orientation << histogram_bin[i]*(1.0/sys.np_range) << ' ' ;
		}
		sys.fout_orientation << endl;
	}
}

void histogramOrderParameter(System& sys)
{
	vector<int> phi_histogram_bin(101, 0);
	for (int i=0; i<sys.np; i++) {
		if (sys.periodic_boundary || sys.in_window[i]) {
			double op = abs(sys.phi[i]);
			int i_orientation = (int)(op*100);
			phi_histogram_bin[i_orientation] ++;
		}
	}
	for (int i=0; i<101; i++) {
		sys.fout_phihist << phi_histogram_bin[i]*(1.0/sys.np_range) << ' ' ;
	}
	sys.fout_phihist << endl;
}

void outputYaplot_voronoi(System& sys)
{
	int c = 0;
	static bool firsttime = true;
	if (firsttime == true) {
		firsttime = false;
	} else {
		sys.fout_yap << endl;
	}
	for (int i=0; i<sys.np; i++) {
		if (sys.voronoi_vertex1[i].size() >= 3) {
			/* At least triangle
			 */
			if (false) {
				if (sys.in_cluster[i] == false) {
					sys.fout_yap << "@ " << 125 << endl;
				} else {
					double op = arg(sys.phi[i])/(2*M_PI)+0.5;
					int color = (int)(119*op)+3;
					sys.fout_yap << "@ " << color << endl;
				}
			} else {
				double op = 1-abs(sys.phi[i]);
				int color = (int)(119*op)+3;
				sys.fout_yap << "@ " << color << endl;
			}
			sys.fout_yap << "p " << sys.voronoi_vertex1[i].size() << ' ';
			for (int j=0; j<sys.voronoi_next[i].size(); j++) {
				sys.fout_yap << sys.voronoi_vertex1[i][j].x-sys.lx/2  << " 0 " << sys.voronoi_vertex1[i][j].z-sys.lz/2 << ' ';
			}
			sys.fout_yap << endl;
		} else {
			// cerr << "sys.voronoi_vertex1[i].size()" << sys.voronoi_vertex1[i].size() << endl;
			
		}
	}
	//	sys.fout_yap << "@ " << 2 << endl;
	//	sys.fout_yap << "r " << 0.5 << endl;
	//	for (int i=0; i<sys.np; i++) {
	//		if (sys.radius[i] > 1.25) {
	//			sys.fout_yap << "c " << sys.position[i].x-sys.lx/2 << " -0.01 " << sys.position[i].z-sys.lz/2 << endl;
	//		}
	//	}
}

void outputYaplot_cluster(System& sys)
{
	int c = 0;
	static bool firsttime = true;
	if (firsttime == true) {
		firsttime = false;
	} else {
		sys.fout_yap << endl;
	}
	sys.fout_yap << "y 1" << endl;
	for (const auto cluster: sys.clusters) {
		//op = angle_cluster[c++];
		//		int color = (int)(120*op);
		//		sys.fout_yap << "@ " << color+3 << endl;
		for (const auto pp: cluster) {
			double op = arg(sys.phi[pp])/(2*M_PI)+0.5;
			int color = (int)(119*op)+3;
			sys.fout_yap << "@ " << color << endl;
			sys.fout_yap << "r " << sys.radius[pp] << endl;
			sys.fout_yap << "c " << sys.position[pp].x-sys.lx/2 << " 0 " << sys.position[pp].z-sys.lz/2 << endl;
			//			sys.fout_yap << "t " << sys.position[pp].x-sys.lx/2 << " 1 " << sys.position[pp].z-sys.lz/2 << ' ' << cluster.size() << endl;
		}
	}
	sys.fout_yap << "y 2" << endl;
	sys.fout_yap << "@ 125" << endl;
	sys.fout_GB << "r 0.5" << endl;
	for (int i=0; i<sys.np; i++) {
		if (sys.in_cluster[i] == false) {
			sys.fout_yap << "r " << sys.radius[i] << endl;
			sys.fout_yap << "c " << sys.position[i].x-sys.lx/2 << " 0 " << sys.position[i].z-sys.lz/2 << endl;
			sys.fout_GB  << "c " << sys.position[i].x-sys.lx/2 << " 0 " << sys.position[i].z-sys.lz/2 << endl;
		}
	}
	
	static int count_fout_GB = 0;
	if (count_fout_GB++ > 20){
		sys.fout_GB << endl;
		count_fout_GB = 0;
	}
	if (sys.draw_square_cell) {
		sys.fout_yap << "y 4" << endl;
		sys.fout_yap << "r 0.2" << endl;
		sys.fout_yap << "@ 15" << endl;
		for (int i=0; i<sys.np; i++) {
			for (const auto j : sys.bond_crystal[i]) {
				vec3d diff_vec = sys.position[i]-sys.position[j];
				if (diff_vec.norm() < 10) {
					sys.fout_yap << "s " << sys.position[i].x-sys.lx/2 << " -0.01 " << sys.position[i].z-sys.lz/2;
					sys.fout_yap << "  " << sys.position[j].x-sys.lx/2 << " -0.01 " << sys.position[j].z-sys.lz/2;
					sys.fout_yap << endl;
				}
			}
		}
	}
	
	if (sys.draw_voronoi_cell) {
		sys.fout_yap << "y 3" << endl;
		sys.fout_yap << "@ 2" << endl;
		for (int i=0; i<sys.np; i++) {
			//			for (int j=0; j<sys.voronoi_next[i].size(); j++) {
			//				int i_next = sys.voronoi_next[i][j];
			//				//vec3d diff_vec = sys.position[i]-sys.position[i_next];
			//				if (diff_vec.norm() < 10) {
			//					//cout << "s " << sys.position[i].x-sys.lx/2 << " 0 " << sys.position[i].z-sys.lz/2 << " ";
			//					//				cout << sys.position[i_next].x-sys.lx/2 << " 0 " << sys.position[i_next].z-sys.lz/2 << endl;
			//					sys.fout_yap  << "l " << sys.voronoi_vertex1[i][j].x -sys.lx/2 << " 0 " << sys.voronoi_vertex1[i][j].z -sys.lz/2;
			//					sys.fout_yap  << " " << sys.voronoi_vertex2[i][j].x -sys.lx/2 << " 0 " << sys.voronoi_vertex2[i][j].z -sys.lz/2 << endl;
			//				}
			//			}
			
			
			//			int color = (int)(120*drand48());
			//			sys.fout_yap << "@ " << color << endl;
			
			for (int j=0; j<sys.voronoi_vertex1[i].size(); j++) {
				//int i_next = sys.voronoi_next[i][j];
				//vec3d diff_vec = sys.voronoi_vertex1[j]-sys.voronoi_vertex2[j];
				//if (diff_vec.norm() < 10) {
				//cout << "s " << sys.position[i].x-sys.lx/2 << " 0 " << sys.position[i].z-sys.lz/2 << " ";
				//				cout << sys.position[i_next].x-sys.lx/2 << " 0 " << sys.position[i_next].z-sys.lz/2 << endl;
				sys.fout_yap  << "l " << sys.voronoi_vertex1[i][j].x -sys.lx/2 << " 0 " << sys.voronoi_vertex1[i][j].z -sys.lz/2 ;
				sys.fout_yap  << " " << sys.voronoi_vertex2[i][j].x -sys.lx/2  << " 0 " << sys.voronoi_vertex2[i][j].z -sys.lz/2 << endl;
				//}
			}
			
		}
	}
	//
	//	sys.fout_yap << "y 3" << endl;
	//	sys.fout_yap << "@ 2" << endl;
	//	for (int i=0; i<sys.np; i++) {
	//		for (int j=0; j<sys.voronoi_next[i].size(); j++) {
	//			int i_next = sys.voronoi_next[i][j];
	//			vec3d diff_vec = sys.position[i]-sys.position[i_next];
	//			if (diff_vec.norm() < 10) {
	//				//cout << "s " << sys.position[i].x-sys.lx/2 << " 0 " << sys.position[i].z-sys.lz/2 << " ";
	//				//				cout << sys.position[i_next].x-sys.lx/2 << " 0 " << sys.position[i_next].z-sys.lz/2 << endl;
	//				sys.fout_yap  << "l " << sys.voronoi_vertex1[i][j].x -sys.lx/2 << " 0 " << sys.voronoi_vertex1[i][j].z -sys.lz/2;
	//				sys.fout_yap  << " " << sys.voronoi_vertex2[i][j].x -sys.lx/2 << " 0 " << sys.voronoi_vertex2[i][j].z -sys.lz/2 << endl;
	//				}
	//		}
	//	}
	
	
	
	
	//	sys.fout_yap << "y 3" << endl;
	//	for (int i=0; i<sys.np; i++) {
	//		for (const auto j : sys.bond[i]) {
	//			bool drawn = false;
	//			for (const auto jj : sys.bond_crystal[i]){
	//				if (j == jj) {
	//					drawn = true;
	//					break;
	//				}
	//			}
	//			if (drawn == false) {
	//				vec3d diff_vec = sys.position[i]-sys.position[j];
	//				if (diff_vec.norm() < 10) {
	//					sys.fout_yap << "l " << sys.position[i].x-sys.lx/2 << " -0.01 " << sys.position[i].z-sys.lz/2;
	//					sys.fout_yap << "  " << sys.position[j].x-sys.lx/2 << " -0.01 " << sys.position[j].z-sys.lz/2;
	//					sys.fout_yap << endl;
	//				}
	//			}
	//		}
	//	}
	//	sys.fout_yap << "y 4" << endl;
	//	sys.fout_yap << "@ 2" << endl;
	//	sys.fout_yap << "r 0.5" << endl;
	//	for (int i=0; i<sys.np; i++) {
	//		if (sys.all_movable[sys.i_time_current][i] == false) {
	//			sys.fout_yap << "c " << sys.position[i].x-sys.lx/2 << " -0.02 " << sys.position[i].z-sys.lz/2 << endl;
	//		}
	//	}
	
	if (sys.i_time_current == 100) {
		for (int i=0; i<sys.np; i++) {
			cout << sys.position[i].x-sys.lx/2 << " " << sys.position[i].z-sys.lz/2 << endl;
		}
	}
	
	//	sys.fout_yap << endl;
}

void outputYaplot_glassy(System& sys) {
	int c = 0;
	vector <bool> not_cluster(sys.np, true);
	for (const auto cluster: sys.clusters) {
		for (const auto pp: cluster) {
			not_cluster[pp] = false;
		}
	}
	sys.fout_GB << "r 0.5" << endl;
	sys.fout_GB << "@ 2" << endl;
	for (int i=0; i<sys.np; i++) {
		if (not_cluster[i]) {
			sys.fout_GB  << "c " << sys.position[i].x-sys.lx/2 << " 0 " << sys.position[i].z-sys.lz/2 << endl;
		}
	}
	static int count_fout_GB = 0;
	if (count_fout_GB++ > 10){
		sys.fout_GB << endl;
		count_fout_GB = 0;
	}
}

void calcPhi4(System& sys)
{
	sys.phi.clear();
	sys.phi.resize(sys.np);
	//sys.in_window.clear();
	//	sys.in_window.resize(sys.np);
	for (int i=0; i<sys.np; i++) {
		if (sys.periodic_boundary || true ) {
			//Box* b = sys.boxset.WhichBox(sys.position[i]);
			//vector<int> list_near = b->neighborhood_container;
			complex<double> phi(0, 0);
			int n_neighobr = 0;
			//for (const auto& j : list_near) {
			for (const auto&j : sys.voronoi_next[i]) {

				if (i != j) {
					bool op_next = true;
					if (sys.ptype_check) {
						if (sys.ptype[i] == sys.ptype[j]) {
							op_next = false;
						} else {
							op_next = true;
						}
					}
					if (op_next) {
						vec3d vec = sys.position[j]-sys.position[i];
						if (sys.periodic_boundary) {
							vec.periodicBoundaryDiff(sys.lx, 0, sys.lz);
						}
						double phase = vec.angle_elevation_xz();
						phi += complex<double> (cos(4*phase), sin(4*phase));
						n_neighobr++;
					}

					//}
				}
			}
			if (n_neighobr >= sys.nn_include_phi) {
				sys.phi[i] = (1.0/n_neighobr)*phi;
			} else {
				sys.phi[i] = 0;
			}
			
		}
	}
	return;
}

void calcPhi6(System& sys)
{
	sys.phi.clear();
	sys.phi.resize(sys.np);
	for (int i=0; i<sys.np; i++) {
		if (true || sys.periodic_boundary) {
			//Box* b = sys.boxset.WhichBox(sys.position[i]);
			//vector<int> list_near = b->neighborhood_container;
			complex<double> phi(0, 0);
			int n_neighobr = 0;
			//for (const auto& j : list_near) {
			if (sys.voronoi_next[i].size() > 0 ) {
				for (const auto&j : sys.voronoi_next[i]) {
					if (i != j) {
						vec3d vec = sys.position[j]-sys.position[i];
						//vec.periodicBoundaryDiff(sys.lx, 0, sys.lz);
						//sys.periodicBoundaryDiffLE(vec);
						double phase = vec.angle_elevation_xz();
						phi += complex<double> (cos(6*phase), sin(6*phase));
						n_neighobr++;
					}
				}
				sys.phi[i] = (1.0/n_neighobr)*phi;
			}
		}
	}
	return;
}



void calcPhi4CorrelationBond(System& sys)
{
	sys.bond.clear();
	sys.bond.resize(sys.np);
	for (int i=0; i<sys.np; i++) {
		if (true || sys.periodic_boundary || sys.in_window[i]) {
			//Box* b = sys.boxset.WhichBox(sys.position[i]);
			//vector<int> list_near = b->neighborhood_container;
			//for (const auto& j : list_near) {
			
			for (const auto&j : sys.voronoi_next[i]) {
				if (sys.ptype[i] != sys.ptype[j]) {
					double correlation = real(sys.phi[i]*conj(sys.phi[j]));
					if (correlation > sys.correlation_threshold) {
						sys.bond[i].push_back(j);
					}
				}
			}
		}
	}
}

void calcAveragePhi4(System& sys)
{
	double total_abs_phi = 0;
	sys.np_range = 0;
	double area = 0;
	for (int i=0; i<sys.np; i++) {
		if (sys.periodic_boundary || sys.in_window[i]) {
			
			total_abs_phi += abs(sys.phi[i]);
			area += M_PI*sys.radius[i]*sys.radius[i];
			sys.np_range ++;
		}
	}
	cerr << "phi total = " << total_abs_phi <<  "np = " << sys.np_range << endl;
	
	//	double total_abs_phi4_2 = 0;
	//	for (const auto cluster: sys.clusters) {
	//		for (const auto pp: cluster) {
	//			if (sys.periodic_boundary || sys.in_window[pp] ) {
	//				total_abs_phi4_2 += abs(sys.phi4[pp]);
	//			}
	//		}
	//	}
	static bool first_time = true;
	double area_fraction = area/sys.area_of_box();
	double current_time = sys.time_list[sys.i_time_current];
	if (first_time) {
		first_time = false;
		sys.fout << "# 1:current_time" << endl;
		sys.fout << "# 2:average abs_phi" << endl;
		sys.fout << "# 3:simple average cluster size" << endl;
		sys.fout << "# 4:averaage sum(n*s**2)/sum(n*s)" << endl;
		sys.fout << "# 5:sys.largest_crystal" << endl;
		sys.fout << "# 6:number_of_particles_crystal/np_range" << endl;
		sys.fout << "# 7:sys.np" << endl;
		sys.fout << "# 8:np_range" << endl;
		sys.fout << "# 9:area_fraction" << endl;
		sys.fout << "# 10:sys.clusters.size()*(1.0/np_range)" << endl;
	}
	sys.fout << current_time << ' '; //1
	sys.fout << total_abs_phi/sys.np_range << ' '; //2
	sys.fout << sys.average_cluster_size << ' '; //3
	sys.fout << sys.average_cluster_size2 << ' '; //4
	sys.fout << sys.largest_crystal << ' ';//5
	sys.fout << sys.number_of_particles_crystal*(1.0/sys.np_range)<< ' ';//6
	sys.fout << sys.np << ' ';//7
	sys.fout << sys.np_range << ' ';//8
	sys.fout << area_fraction << ' ';//9
	sys.fout << sys.clusters.size()*(1.0/sys.np_range) << ' ';
	sys.fout << (1.0*sys.np_mag) / (sys.np_mag +sys.np_nonmag) << endl;
}

void outputYaplot_OP(System& sys,
					 int yaplot_order_parameter)
{
	int color;
	double op;
	static bool firsttime = true;
	if (firsttime == true) {
		firsttime = false;
	} else {
		sys.fout_yap << endl;
	}
//	if (sys.time_list[sys.i_time_current] > 400) {
//		ofstream fout("hoge.dat");
//		for (int i=0; i<sys.np; i++) {
//			fout << sys.position[i].x-sys.lx/2 << " " << sys.position[i].z-sys.lz/2 << " " << abs(sys.phi[i]) << endl;
//		}
//		fout.close();
//		ofstream fout2("voro.dat");
//		for (int i=0; i<sys.np; i++) {
//			double op = abs(sys.phi[i]);
//			fout2 << op << ' ';
//			for (int j=0; j<sys.voronoi_next[i].size(); j++) {
//				fout2 << sys.voronoi_vertex1[i][j].x -sys.lx/2  << ' ' << sys.voronoi_vertex1[i][j].z -sys.lz/2<< ' ' ;
//			}
//			fout2 << endl;
//			
//		}
//
//		exit(1);
//	}
//	return;
	sys.fout_yap << "y 1" << endl;
	sys.fout_yap << "r " << 1 << endl;
	for (int i=0; i<sys.np; i++) {
		if (sys.periodic_boundary || true) {
			if (abs(sys.phi[i]) == 0) {
				op = 1;
			} else {
				if (yaplot_order_parameter == 0) {
					op = 1-abs(sys.phi[i]);
				} else {
					//op = (arg(sys.phi[i])+sys.shear_strain*0.5)/(2*M_PI)+0.5;
					op = (arg(sys.phi[i]))/(2*M_PI)+0.5;
					//					if ( i % 10 == 0) {
					//						cout << sys.shear_strain << ' ' << arg(sys.phi[i]) + M_PI  << endl;
					//					}
				}
			}
			//if (abs(arg(sys.phi[i])) > M_PI/2) {
			if (abs(sys.phi[i]) > 0) {
				color = (int)(119*op)+3;
				// from 3 to 12
				sys.fout_yap << "@ " << color << endl;
				//				sys.fout_yap << "r " << sys.radius[i] << endl;
				sys.fout_yap << "c " << sys.position[i].x-sys.lx/2 << " 0 " << sys.position[i].z-sys.lz/2 << endl;
				//}
			} else {
				// from 3 to 12
				sys.fout_yap << "@ " << 0 << endl;
				//				sys.fout_yap << "r " << sys.radius[i] << endl;
				sys.fout_yap << "c " << sys.position[i].x-sys.lx/2 << " 0 " << sys.position[i].z-sys.lz/2 << endl;
			}
		}
			
	}
//	sys.fout_yap << "y 5" << endl;
//	sys.fout_yap << "@ " << 2 << endl;
//	sys.fout_yap << "r " << 2 << endl;
//	sys.fout_yap << "s " << -sys.lx/2 + sys.shear_disp/2 << " 0 " << sys.lz/2 << ' ' << -sys.lx/2 + sys.shear_disp/2 << " 0 " << sys.lz/2 + 5 << endl;
//	sys.fout_yap << "s " << 0 + sys.shear_disp/2 << " 0 " << sys.lz/2 << ' ' << 0 + sys.shear_disp/2 << " 0 " << sys.lz/2 + 5 << endl;
//	sys.fout_yap << "s " << sys.lx/2 - sys.shear_disp/2 << " 0 " << -sys.lz/2 << ' ' << sys.lx/2 - sys.shear_disp/2 << " 0 " << -sys.lz/2 - 5 << endl;
//	sys.fout_yap << "s " << 0 - sys.shear_disp/2 << " 0 " << -sys.lz/2 << ' ' << 0 - sys.shear_disp/2 << " 0 " << -sys.lz/2 - 5 << endl;

	
	
	if (sys.draw_square_cell) {
		sys.fout_yap << "y 4" << endl;
		sys.fout_yap << "@ 2" << endl;
		sys.fout_yap << "r 0.2" << endl;
		for (int i=0; i<sys.np; i++) {
			for (const auto j : sys.bond_crystal[i]) {
				vec3d diff_vec = sys.position[i]-sys.position[j];
				if (diff_vec.norm() < 10) {
					sys.fout_yap << "s " << sys.position[i].x-sys.lx/2 << " -0.01 " << sys.position[i].z-sys.lz/2;
					sys.fout_yap << "  " << sys.position[j].x-sys.lx/2 << " -0.01 " << sys.position[j].z-sys.lz/2;
					sys.fout_yap << endl;
				}
			}
		}
	}
	if (sys.draw_voronoi_cell) {
		sys.fout_yap << "y 3" << endl;
		//sys.fout_yap << "@ 2" << endl;
		for (int i=0; i<sys.np; i++) {
			
			for (int j=0; j<sys.voronoi_next[i].size(); j++) {
				int i_next = sys.voronoi_next[i][j];
				vec3d diff_vec = sys.position[i]-sys.position[i_next];
				if (diff_vec.norm() < 10) {
					//cout << "s " << sys.position[i].x-sys.lx/2 << " 0 " << sys.position[i].z-sys.lz/2 << " ";
					//				cout << sys.position[i_next].x-sys.lx/2 << " 0 " << sys.position[i_next].z-sys.lz/2 << endl;
					sys.fout_yap  << "l " << sys.voronoi_vertex1[i][j].x -sys.lx/2 << " 0 " << sys.voronoi_vertex1[i][j].z -sys.lz/2;
					sys.fout_yap  << " " << sys.voronoi_vertex2[i][j].x -sys.lx/2 << " 0 " << sys.voronoi_vertex2[i][j].z -sys.lz/2 << endl;
				}
			}
		}
	}
	
	
	//	sys.fout_yap << "y 3" << endl;
	//	sys.fout_yap << "@ 2" << endl;
	//	sys.fout_yap << "r 0.05" << endl;
	//	for (int i=0; i<sys.np; i++) {
	//		for (int j=0; j<sys.voronoi_next[i].size(); j++) {
	//			int i_next = sys.voronoi_next[i][j];
	//			if (sys.ptype[i] != sys.ptype[i_next]) {
	//				sys.fout_yap << "s " << sys.position[i].x-sys.lx/2 << " -0.01 " << sys.position[i].z-sys.lz/2 << " ";
	//				sys.fout_yap << sys.position[i_next].x-sys.lx/2 << " -0.01 " << sys.position[i_next].z-sys.lz/2 << endl;
	//			}
	//		}
	//	}
}

void outputPhi4Config(System& sys)
{
	sys.fout_OPconfig << sys.time_list[sys.i_time_current] << endl;
	for (int i=0; i<sys.np; i++) {
		if (sys.periodic_boundary || true) {
			double abs_op = abs(sys.phi[i]);
			double arg_op = arg(sys.phi[i]);
			sys.fout_OPconfig << sys.position[i].x << ' ' << sys.position[i].z << ' ';
			sys.fout_OPconfig << abs_op << ' ' << arg_op <<  ' ' << sys.ptype[i] << endl;
		}
	}
}

void cout_yaplot(System& sys)
{
	cout << "y 1" << endl;
	cout << "@ 125" << endl;
	for (int i=0; i<sys.np; i++) {
		cout << "r " << sys.radius[i] << endl;
		cout << "c " << sys.position[i].x-sys.lx/2 << " 0.01 " << sys.position[i].z-sys.lz/2 << endl;
	}
	cout << "y 3" << endl;
	cout << "@ 125" << endl;
	for (int i=0; i<sys.np; i++) {
		for (const auto j : sys.bond[i]) {
			vec3d diff_vec = sys.position[i] - sys.position[j];
			if (diff_vec.norm() < 10) {
				cout << "l " << sys.position[i].x-sys.lx/2 << " 0.01 " << sys.position[i].z-sys.lz/2;
				cout << "  " << sys.position[j].x-sys.lx/2 << " 0.01 " << sys.position[j].z-sys.lz/2;
				cout << endl;
			}
		}
	}
	cout << "y 2" << endl;
	cout << "@ 2" << endl;
	for (int i=0; i<sys.np; i++) {
		for (auto j : sys.bond_crystal[i]) {
			vec3d diff_vec = sys.position[i] - sys.position[j];
			if (diff_vec.norm() < 10) {
				cout << "l " << sys.position[i].x-sys.lx/2 << " 0 " << sys.position[i].z-sys.lz/2;
				cout << "  " << sys.position[j].x-sys.lx/2 << " 0 " << sys.position[j].z-sys.lz/2;
				cout << endl;
			}
		}
	}
	cout << endl;
}

#endif /* OrderParameter_h */
