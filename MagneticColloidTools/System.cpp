//
//  System.cpp
//  gbanalyzer
//
//  Created by Ryohei Seto on 2015/10/05.
//  Copyright © 2015年 Ryohei Seto. All rights reserved.
//

#include "System.hpp"

void System::initializeBoxing()
{
	/**
	 \brief Initialize the boxing system.
	 
	 Initialize the BoxSet instance using as a minimal Box size the maximal interaction range between any two particles in the System.
	 */
	boxset.init(box_size, this);
	for (int i=0; i<np; i++) {
		boxset.box(i);
	}
	boxset.update();
}


int System::importData(ifstream& input_data)
{
	std::string buf[6];
	input_data >> buf[0] >> buf[1];
	input_data >> buf[0] >> buf[1];
	time_list.push_back(atof(buf[1].c_str()));
	if (input_data.eof()) {
		cerr << "EOF" << endl;
		return 1;
	}
	std::vector <vec3d> pos;
	std::vector <int> particle_type;
	std::vector <double> radi;
	std::vector <bool> import_movable;
	input_data >> buf[0] >> buf[1];
	int num_of_mag_particle = atoi(buf[1].c_str());
	if (buf[0] != "number_of_magnetic_particles"
		&& buf[0] != "np") {
		cerr << "not number_of_magnetic_particles" << endl;
		exit(1);
	}
	
	np_mag = num_of_mag_particle;
	for (int i=0; i<num_of_mag_particle; i++) {
		input_data >> buf[0] >> buf[1] >> buf[2] >> buf[3];
		vec3d p(atof(buf[0].c_str()), 0, atof(buf[1].c_str()));
		double a = atof(buf[2].c_str());
		int pt = atoi(buf[3].c_str());
		radi.push_back(a);
		pos.push_back(p);
		bool val_movable = true;
		if (pt > 50) {
			pt -= 100;
			val_movable = false;
		}
		particle_type.push_back(pt);
		import_movable.push_back(val_movable);
	}
	input_data >> buf[0] >> buf[1];
	
	if (buf[0] != "number_of_nonmagnetic_particles") {
		cerr << "not number_of_nonmagnetic_particles" << endl;
		exit(1);
	}

	int num_of_nonmag_particle = atoi(buf[1].c_str());
	np_nonmag = num_of_nonmag_particle;

	for (int i=0; i<num_of_nonmag_particle; i++) {
		input_data >> buf[0] >> buf[1] >> buf[2] >> buf[3];
		vec3d p(atof(buf[0].c_str()), 0, atof(buf[1].c_str()));
		double a = atof(buf[2].c_str());
		int pt = atoi(buf[3].c_str());
		//
		radi.push_back(a);
		pos.push_back(p);
		bool val_movable = true;
		if (pt > 50) {
			pt -= 100;
			val_movable = false;
		}
		particle_type.push_back(pt);
		import_movable.push_back(val_movable);
	}
	//		cout << endl;
	//all_pos.push_back(pos);
	//all_ptype.push_back(particle_type);
	//all_radius.push_back(radi);
	//all_movable.push_back(import_movable);
	
	position.clear();
	ptype.clear();
	radius.clear();
	np = (int)pos.size();
	position.resize(np);
	ptype.resize(np);
	radius.resize(np);
	for (int i=0; i<np; i++) {
		position[i] = pos[i];
		ptype[i] = particle_type[i];
		radius[i] = radi[i];
	}
	return 0;
}

void System::periodize(vec3d& pos)
{
	/* Lees-Edwards boundary condition
	 *
	 */
	if (pos.z >= lz) {
		pos.z -= lz;
		pos.x -= shear_disp;
	} else if (pos.z < 0) {
		pos.z += lz;
		pos.x += shear_disp;
	}
	while (pos.x >= lx) {
		pos.x -= lx;
	}
	while (pos.x < 0) {
		pos.x += lx;
	}
}

void System::periodicBoundaryDiffLE(vec3d& vec)
{
	/* Lees-Edwards boundary condition
	 *
	 */
	if (vec.z >= lz_half) {
		vec.z -= lz;
		vec.x -= shear_disp;
	} else if (vec.z < -lz_half) {
		vec.z += lz;
		vec.x += shear_disp;
	}
	while (vec.x >= lx_half) {
		vec.x -= lx;
	}
	while (vec.x < -lx_half) {
		vec.x += lx;
	}
}

int System::importDataShear(ifstream& input_data)
{
	bool shear = false;
	std::string buf[6];
	std::vector <vec3d> pos;
	std::vector <double> radi;
	if (shear) {
		input_data >> buf[0] >> buf[1];
		input_data >> buf[0] >> buf[1];
		shear_strain = atof(buf[1].c_str());
		input_data >> buf[0] >> buf[1];
		shear_disp = atof(buf[1].c_str());
		if (input_data.eof()) {
			cerr << "EOF" << endl;
			exit(1);
		}
		input_data >> buf[0] >> buf[1];
		int num_particle = atoi(buf[1].c_str());
		if (buf[0] != "np") {
			cerr << buf[0] << endl;
			cerr << "not np (1)" << endl;
			exit(1);
		}
	} else {
		input_data >> buf[0] >> buf[1];
		input_data >> buf[0] >> buf[1];
		if (buf[0] == "strain") {
			input_data >> buf[0] >> buf[1];
			input_data >> buf[0] >> buf[1];
		}

		int num_particle = atoi(buf[1].c_str());
		np = num_particle;
		if (buf[0] != "number_of_particles" && buf[0] != "np") {
			cerr << buf[0] << endl;
			cerr << "not np (2)" << endl;
			exit(1);
		}
	}
	if (np <= 0) {
		cerr << np << endl;
		exit(1);
	}
	for (int i=0; i<np; i++) {
		input_data >> buf[0] >> buf[1] >> buf[2] ;
		vec3d p(atof(buf[0].c_str()), 0, atof(buf[1].c_str()));
		double a = atof(buf[2].c_str());
		radi.push_back(a);
		pos.push_back(p);
	}
	position.clear();
	radius.clear();
	np = (int)pos.size();
	position.resize(np);
	radius.resize(np);
	for (int i=0; i<np; i++) {
		position[i] = pos[i];
		radius[i] = radi[i];
	}
	return 0;
}


int System::importData_exdata(ifstream& input_data)
{
	static bool first_time = true;
	std::vector <vec3d> pos;
	std::vector <int> particle_type;
	std::vector <double> radi;
	std::string buf[6];
	input_data >> buf[0] >> buf[1];
	int frame_number = atoi(buf[1].c_str());
	time_list.push_back(0.5*frame_number);
	if (input_data.eof()) {
		cerr << "EOF" << endl;
		return 1;
	}
	cerr << buf[0] << ' ' << buf[1]<< endl;
	input_data >> buf[0] >> buf[1];
	int num_of_mag_particle = atoi(buf[1].c_str());
	double radius_magnetic = 11;
	double radius_non_magnetic = 12.1;

	cerr << "num_of_mag_particle = " << num_of_mag_particle << endl;
	np_mag = num_of_mag_particle;
	for (int i=0; i<num_of_mag_particle; i++) {
		//input_data >> buf[0] >> buf[1] >> buf[2] >> buf[3] >> buf[4];
		input_data >> buf[0] >> buf[1] ;
		vec3d p(atof(buf[0].c_str())/radius_magnetic, 0, atof(buf[1].c_str())/radius_magnetic);
		double a = 1;
		int pt = 1;
		//
		bool duplicated = false;
		for (auto pp : pos) {
			double sq_dist = (pp-p).sq_norm();
			if (sq_dist < 0.01) {
				duplicated = true;
				break;
			}
		}
		if (duplicated == false ) {
			radi.push_back(a);
			pos.push_back(p);
			particle_type.push_back(pt);
		}
	}
	input_data >> buf[0] >> buf[1];
	
	int num_of_nonmag_particle = atoi(buf[1].c_str());
	cerr << "num_of_nonmag_particle = " << num_of_nonmag_particle << endl;
	np_nonmag = num_of_nonmag_particle;
	for (int i=0; i<num_of_nonmag_particle; i++) {
		//input_data >> buf[0] >> buf[1] >> buf[2] >> buf[3] >> buf[4];
		input_data >> buf[0] >> buf[1] ;
		vec3d p(atof(buf[0].c_str())/radius_magnetic, 0, atof(buf[1].c_str())/radius_magnetic);
		//double a = atof(buf[2].c_str());
		double a = radius_non_magnetic/radius_magnetic;
		//			int pt = atoi(buf[3].c_str());
		//int pt = atoi(buf[3].c_str());
		int pt = -1;
		//
		bool duplicated = false;
		for (auto pp : pos) {
			double sq_dist = (pp-p).sq_norm();
			if (sq_dist < 0.01) {
				duplicated = true;
				break;
			}
		}
		if (duplicated == false ) {
			radi.push_back(a);
			pos.push_back(p);
			particle_type.push_back(pt);
		}
	}
	if (first_time) {
		first_time = false;
		double x_min = 100;
		double x_max = 0;

		double z_min = 100;
		double z_max = 0;
		for (auto p: pos) {
			if (p.x < x_min) {
				x_min = p.x;
			} else if (p.x > x_max) {
				x_max = p.x;
			}
			if (p.z < z_min) {
				z_min = p.z;
			} else if (p.z > z_max) {
				z_max = p.z;
			}
		}
		
		if (lx == 0) {
			lx = x_max;
			lz = z_max;
			lx_half = lx/2;
			lz_half = ly/2;

			cerr << "lx lz" << lx << ' ' << lz << endl;
		}
	}
	position.clear();
	ptype.clear();
	radius.clear();
	np = (int)pos.size();
	cerr << "np = " << np << endl;
	position.resize(np);
	ptype.resize(np);
	radius.resize(np);
	for (int i=0; i<np; i++) {
		position[i] = pos[i];
		ptype[i] = particle_type[i];
		radius[i] = radi[i];
	}
	cerr << "--" << endl;
	
	return 0;
	//	int ii = (int)(0.5*all_pos.size());
//	for (int i = ii; i<(int)all_pos.size(); i++){
//		for (auto pp : all_pos[i]) {
//			cout << pp.x << ' ' << pp.z << endl;
//		}
//		
//	}
//	exit(0);
}

