//
//  main.cpp
//  MagneticColloidTools
//
//  Created by Ryohei Seto on 2015/10/14.
//  Copyright © 2015年 Ryohei Seto. All rights reserved.

#include <iostream>
#include <stdexcept>
#include <string>
#include <algorithm>
#include <ostream>
#include <string>
#include <getopt.h>
#include "Tracking.h"
#include "GBdomain.h"
#include "OrderParameter.h"
#include "PairCorrelationFunction.h"
#define no_argument 0
#define required_argument 1
#define optional_argument 2
using namespace std;


int main(int argc, char** argv) {
	if (argc == 1) {
		//cerr << "-t: tracking experimental data" << endl;
		cerr << "-o: calc order parameter" << endl;
		//cerr << "-g: analyze grain boundary" << endl;
		return 0;
	}
	const struct option longopts[] = {
		//{"tracking", required_argument, 0, 't'},
		{"order-parameter", required_argument, 0, 'o'},
		//{"grain-boundary", required_argument, 0, 'g'},
		{"average-number", required_argument, 0, 'a'},
		{"particle-type-check", no_argument, 0, 'p'},
		{"bond-order-threshold", required_argument, 0, 'b'},
		{"box-size", required_argument, 0, 'r'},
		{"order-parameter-yaplot", required_argument, 0, 'y'}
		
	};
	int tool_option = 0;
	int index;
	int c;
	int average_number = 0;
	double bond_order_trheshold = 0.5;
	double box_size = 6;
	int nn_include_phi4 = 2;
	
	bool particle_type_check = false;
	bool draw_voronoi_cell = false;
	bool draw_square_cell = false;
	
	string import_filename;
	/* yaplot_order_parameter
	 * -1: no output, 0: absolute value, 1: argument
	 */
	int yaplot_order_parameter = -1;
	
	while ((c = getopt_long(argc, argv, "t:o:g:c:y:a:b:r:n:pvs", longopts, &index)) != -1) {
		switch (c) {
			case 't':
				if (tool_option == 0) {
					tool_option = 1;
					import_filename = optarg;
				} else {
					cerr << "..." << endl;
					exit(1);
				}
				break;
			case 'o':
				if (tool_option == 0) {
					tool_option = 2;
					import_filename = optarg;
				} else {
					cerr << "..." << endl;
					exit(1);
				}
				break;
			case 'g':
				if (tool_option == 0) {
					tool_option = 3;
					import_filename = optarg;
				} else {
					cerr << "..." << endl;
					exit(1);
				}
				break;
			case 'c':
				if (tool_option == 0) {
					tool_option = 4;
					import_filename = optarg;
				} else {
					cerr << "..." << endl;
					exit(1);
				}
				break;
			case 'a':
				average_number = atoi(optarg);
				break;
			case 'b':
				bond_order_trheshold = atof(optarg);
				break;
			case 'r':
				box_size = atof(optarg);
				break;
			case 'n':
				nn_include_phi4 = atoi(optarg);
				break;

			case 'p':
				particle_type_check = true;
				break;
				
			case 'v':
				draw_voronoi_cell = true;
				break;

			case 's':
				draw_square_cell = true;
				break;

			case 'y':
				string optarg_str = optarg;
				if (optarg_str == "abs") {
					yaplot_order_parameter = 0;
				} else if (optarg_str == "arg") {
					yaplot_order_parameter = 1;
				} else if (optarg_str == "cluster") {
					yaplot_order_parameter = 2;
				} else if (optarg_str == "glassy") {
					yaplot_order_parameter = 3;
				} else if (optarg_str == "voronoi") {
					yaplot_order_parameter = 4;
				} else {
					cerr << optarg << endl;
					cerr << "-y abs to output absolute value of order parameter" << endl;
					cerr << "-y arg to output argument of order parameter" << endl;
					exit(1);
				}
				break;
		}
	}
	switch (tool_option) {
		case 1:
			tracking_exdata(import_filename, average_number);
			break;
		case 2:
			OrderParameter(import_filename,
						   particle_type_check,
						   bond_order_trheshold,
						   yaplot_order_parameter,
						   draw_voronoi_cell,
						   draw_square_cell,
						   box_size,
						   nn_include_phi4);
			break;
		case 3:
			cerr << "Use OrderParameter() for Grain boundary / Crystal domain indentification\n";
			//gbdomain();
			break;
		case 4:
			PairCorrelationFunction(import_filename);
			break;

	}
	return 0;
}
