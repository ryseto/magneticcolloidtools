//
//  Tracking.h
//  gbanalyzer
//
//  Created by Ryohei Seto on 2015/10/07.
//  Copyright © 2015年 Ryohei Seto. All rights reserved.
//

#ifndef Tracking_h
#define Tracking_h
#include <vector>
#include <map>
#include "System.hpp"
#include "vec3d.h"
#include "BoxSet.hpp"
#include "Box.hpp"
#include "Particle.hpp"
using namespace std;

const double tracking_min_distance = 2;

void tracking(System& sys, int i_time, vector<int>& orphaned_particles);

void outputYaplot(System& sys, int i_time)
{
	for (auto& p : sys.particles) {
		if (p->time.back() == i_time) {
			if (p->time.size() == 1) {
				cout << "@ 3" << endl;
			} else {
				if (p->particle_type) {
					cout << "@ 2" << endl;
				} else {
					cout << "@ 6" << endl;
				}
			}
			cout << "c " << p->position.back().x << " 0 ";
			cout << " " << p->position.back().z << endl;
		}
	}
//	cout << "@ 4" << endl;
//	cout << "r 0.5" << endl;
//	for (auto p_id : orphaned_particles) {
//		vec3d pos = all_pos[i_time][p_id];
//		cout << "c " << pos.x << " 0 " << pos.z << endl;
//		
//	}
	cout << endl;
}

void find_box_size(System& sys)
{
//	double x_min = 10, x_max = 0;
//	double z_min = 10, z_max = 0;
//	int i_time = 0;
//	int num_of_particle = (int)sys.all_pos[i_time].size();
//
//	for (int i=0; i<num_of_particle; i++) {
//		if (sys.all_pos[i_time][i].x < x_min) {
//			x_min = sys.all_pos[i_time][i].x;
//		} else if (sys.all_pos[i_time][i].x > x_max) {
//			x_max = sys.all_pos[i_time][i].x;
//		}
//		if (sys.all_pos[i_time][i].z < z_min) {
//			z_min = sys.all_pos[i_time][i].z;
//		} else if (sys.all_pos[i_time][i].z > z_max) {
//			z_max = sys.all_pos[i_time][i].z;
//		}
//	}
//	cerr << x_min << " < x < " << x_max << endl;
//	cerr << z_min << " < z < " << z_max << endl;
//	sys.set_box(x_min+1, x_max-1, z_min+1, z_max-1);
}

void output_tracking(System& sys, int i_time)
{
	int num_counter = 0;
	sys.fout << "# " << i_time << " " <<  i_time*0.5 << "\n";
	for (auto p : sys.particles) {
		if (p->active) {
			vec3d pos = p->position_averaged;
			double a;
			if (p->particle_type == 0) {
				a = 1.1;
			} else {
				a = 1;
			}
			sys.fout << pos.x << " " << sys.lz-pos.z << " " << a << " " <<  p->particle_type << endl;
			num_counter++;
		}
	}
	sys.fout << "#" << endl;
	cout << "r 1.1" << endl; // magnetic .. large (?)
	cout << "@ 5" << endl;
	for (auto p : sys.particles) {
		if (p->active) {
			if (p->particle_type == 0) {
				vec3d pos = p->position_averaged;

				cout << "c " << pos.x-sys.lx/2 << " 0.01 " << -(pos.z-sys.lz/2)<< endl;
			}
		}
	}
	cout << "r 1" << endl;
	cout << "@ 2" << endl;
	for (auto p : sys.particles) {
		if (p->active) {
			if (p->particle_type == 1) {
				vec3d pos = p->position_averaged;
				cout << "c " << pos.x-sys.lx/2 << " 0.01 " << -(pos.z-sys.lz/2)<< endl;
			}
		}
	}
	cout << endl;
}

void tracking_exdata(string filename, int average_number)
{
	System sys;
	sys.prepareSimulationName(filename);
	sys.num_average = average_number;
	stringstream outputfilename;
	outputfilename << "config_" << sys.data_name << "_av" << sys.num_average << ".dat";
	cerr << outputfilename.str() << endl;
	sys.fout.open(outputfilename.str());
	//@@@@@@CHanged
	//sys.importData(filename);@@@@@@
	//@@@@@@
	find_box_size(sys);
	int i_time = 0;
	vector <int> suspended_particle;
	int total_num_lost = 0;
	sys.fout << "# " << sys.lx << " " << sys.lz << " " << 0 << endl;
	for (i_time = 1; true ; i_time ++) {
		sys.i_time_current = i_time;
		sys.initializeBoxing();
		vector <vector<int>> tracked(sys.np);
		int num_lost = 0;
		for (auto p : sys.particles) {
			if (p->active) {
				vec3d pos = p->position.back();
				Box* b = sys.boxset.WhichBox(pos);
				vector<int> list_near = b->neighborhood_container;
				double distance_min = tracking_min_distance;
				int near_rest_particle = -1;
				for (const auto& j : list_near) {
					if (p->particle_type == sys.ptype[j]) {
						double distance = (pos-sys.position[j]).norm();
						if (distance < distance_min) {
							distance_min = distance;
							near_rest_particle = j;
						}
					}
				}
				if (near_rest_particle != -1) {
					tracked[near_rest_particle].push_back(p->id);
					double distance = (pos-sys.position[near_rest_particle]).norm();
					p->track_add(sys.position[near_rest_particle],
								 i_time, near_rest_particle, distance);
				} else {
					suspended_particle.push_back(p->id);
					p->trac_lost();
					num_lost ++;
					if (sys.check_in_window(pos)) {
						total_num_lost ++;
					}
				}
			}
		}
		int new_particle = 0;
		for (int i=0; i<sys.np; i++) {
			int num_tracked = (int)tracked[i].size();
			if (num_tracked == 0) {
				if (suspended_particle.empty() == false) {
					int j = suspended_particle.back();
					suspended_particle.pop_back();
					sys.particles[j]->reuse(j,
											sys.position[i],
											sys.ptype[i],
											sys.radius[i],
											i_time);
				} else {
					int k = (int)sys.particles.size();
					sys.particles.push_back(new Particle(k,
														 sys.position[i],
														 sys.ptype[i],
														 sys.radius[i],
														 i_time));
				}
				new_particle++;
			} else if (num_tracked > 1) {
				double min_distance = tracking_min_distance;
				int nearest_particle = -1;
				for (auto t_particle : tracked[i]) {
					if (sys.particles[t_particle]->tracked_distance < min_distance) {
						min_distance = sys.particles[t_particle]->tracked_distance;
						nearest_particle = t_particle;
					}
				}
				for (auto t_particle : tracked[i]) {
					if (t_particle != nearest_particle) {
						sys.particles[t_particle]->trac_lost();
						suspended_particle.push_back(t_particle);
						num_lost ++;
					}
				}
			}
		}
		for (auto p : sys.particles) {
			if (p->active) {
				p->averaging_position(sys.num_average);
			}
		}
		output_tracking(sys, i_time);
		sys.boxset.clear();
		
		cerr << i_time << ' ' << sys.np << endl;
		cerr << "new " << new_particle << endl;
		cerr << "num_lost " << num_lost << endl;
		cerr << " sus "  << suspended_particle.size() << "  " << sys.particles.size() << endl;
	}
	cerr << "total_num_lost = " << total_num_lost << endl;
}

#endif /* Tracking_h */
